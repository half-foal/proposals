$(document).ready(function () {

  let $navbarToggler = $('.js-navbar-toggler');

  let mainMenu = {
    showScrollMenu: function () {
      $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
          $('#js-menu-top').fadeIn(500);
        } else {
          $('#js-menu-top').fadeOut(500);
        }
      });
    },
    onMenuTogglerClick: function (e) {

      if ($(".navbar-toggler").attr('aria-expanded') === "true") {
        // nav menu opened
        mainMenu.hideBody();
      } else {
        //  nav is closed 
        mainMenu.showBody();
      }
    },
    showBody: function () {
      $('body').removeClass('navbar-collapse-open')
    },
    hideBody: function () {
      $('body').addClass('navbar-collapse-open')
    },
    init: function () {
      mainMenu.showScrollMenu(),
        $(document).on('click', $navbarToggler, mainMenu.onMenuTogglerClick);
    }
  }
  mainMenu.init();

  let scrollButton = $('.js-scroll-button');

  let homeTop = {
    onScrollButtonClick: function (e) {

      e.preventDefault();

      let sectionAfterHomeTop = $(scrollButton).closest('.page-section').next();

      console.log(sectionAfterHomeTop);

      $('html, body').animate({
        scrollTop: sectionAfterHomeTop.offset().top
      }, 700);

    },
    init: function () {
      $(scrollButton).on('click', scrollButton, homeTop.onScrollButtonClick);
    }
  }
  homeTop.init();


});